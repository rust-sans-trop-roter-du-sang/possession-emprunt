from manim import *

from code import Heap, Stack, RustCode


class VariableCreation(Scene):
    def construct(self):
        rust = RustCode.from_file("variable_creation.rs")
        self.play(Write(rust))
        self.wait(8)

        self.play(GrowArrow(rust.pointer()))
        self.wait(4)

        self.play(rust.animate.point_to_next_line())
        self.wait(2)
        self.play(rust.animate.to_edge(LEFT))
        self.wait(4)

        heap = Heap().to_edge(RIGHT, buff=2).to_edge(UP, buff=0.1)
        self.play(Write(heap))
        self.wait(18)

        ferris = heap.allocate("Ferris")
        self.play(Write(ferris))
        self.wait(4)

        stack = Stack().to_edge(RIGHT, buff=2).to_edge(DOWN, buff=0.1)
        self.play(Write(stack))
        self.wait(20)

        x = stack.push("len: 6", "start: @")
        self.play(Write(x))
        pointer = x.pointer(1, ferris[0].get_edge_center(DOWN))
        self.play(DrawBorderThenFill(pointer))
        self.play(Write(x.named("x")))
        self.wait(4)

        self.wait(3)
