fn main() {
    let x = String::from("Ferris");
    greet(&x);
    println!("Bye {}", &x);
}

fn greet(y: &String) {
    println!("Hello {}", y);
}