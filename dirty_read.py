from assets import ferris_exclamation
from code import *


class DirtyRead(Scene):
    def construct(self):
        dirty_read = RustCode.from_file("dirty_read.rs")

        self.play(Write(dirty_read))
        self.wait(4)
        self.play(dirty_read.animate.to_edge(LEFT))

        heap = Heap().to_edge(RIGHT, buff=2).to_edge(UP, buff=0.1)
        ferris = heap.allocate("Ferris")
        stack = Stack().to_edge(RIGHT, buff=2).to_edge(DOWN, buff=0.1)
        x = stack.push_named("x", "len: 6", "start: @")
        x_arrow = x.pointer(1, ferris[0].get_edge_center(DOWN))
        x.invalidate()
        y = stack.push_named("y", "len: 6", "start: @")
        y_arrow = y.pointer(1, ferris[0].get_edge_center(DOWN))
        self.play(GrowArrow(dirty_read.point_to_line(3)), *[FadeIn(e) for e in [heap, stack, x_arrow, y_arrow]])
        self.wait(2)

        self.play(dirty_read.animate.point_to_next_line())
        fe = allocated_memory("Fe").align_to(ferris, LEFT + UP)
        self.wait()
        self.play(
            Transform(ferris, fe),
            Transform(y.content_line(0), Element.new_with_name("y", "len: 2", "start: @").move_to(y).content_line(0))
        )
        self.wait(2)

        self.play(dirty_read.animate.point_to_next_line())
        self.wait(3)
        self.play(Circumscribe(x.content_line(0), color=RED_E, buff=0.05))
        self.wait()
        self.play(Circumscribe(fe, color=RED_E))
        self.wait(3)
        self.play(
            FadeIn(ferris_exclamation().next_to(dirty_read.line(5), RIGHT)),
            FadeIn(ferris_exclamation().next_to(x, LEFT))
        )
        self.wait(7)

        self.wait(3)
