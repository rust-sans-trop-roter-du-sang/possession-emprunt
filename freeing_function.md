# Script

Et pour les appels de fonction alors ? C'est la même chose : une fonction n'est pas grand-chose de plus qu'un bloc
de code qui porte un nom. Le passage de la variable x en paramètre déplace la propriété vers la variable y de la
fonction greet. Lorsque celle-ci termine son exécution, la mémoire allouée à la variable y est libérée et, comme juste
avant, il ne faut surtout pas utiliser la variable x. Le compilateur Rust se chargera de vérifier ce point.