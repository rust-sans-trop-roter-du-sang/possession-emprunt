fn main() {
    let x = String::from("Ferris");
    greet(x);
    println!("Bye");
}

fn greet(y: String) {
    println!("Hello {}", &y);
}