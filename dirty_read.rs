fn main() {
    let x = String::from("Ferris");
    let mut y = x;
    y.truncate(2);
    println!("Hello {}", &x);
    println!("Bye {}", &x);
}
