# Script

Nous venons de voir les références simples, c'est-à-dire en lecture seule. Voyons maintenant comment obtenir une
référence mutable.

Premier changement, pour espérer obtenir une référence mutable, il faut d'abord que la variable dont on souhaite 
emprunter les données soit elle-même déclarée comme mutable. Tous les contrôles de mutabilité et d'emprunt étant 
effectués par le compilateur, il n'y a aucun impact sur la représentation en mémoire à l'exécution.

On peut alors créer une référence mutable et la fournir à la fonction greet : on donne les clefs de l'appartement aux 
ouvriers pour qu'ils puissent casser les murs. L'emprunt mutable se termine quand la variable y meurt, à la fin de 
l'exécution de la fonction greet.

Les ouvriers ont rendu les clefs, la variable x peut continuer à être utilisée dans la suite du programme.