from code import *


class Borrowing(Scene):
    def construct(self):
        moving = RustCode.from_file("freeing_function.rs")
        borrowing = RustCode.from_file("borrowing.rs")

        self.play(FadeIn(moving))
        self.wait(2)
        self.play(
            FadeTransform(moving, borrowing, run_time=2),
            *[Circumscribe(e, color=RED_E, buff=0.01, run_time=1.5) for e in [borrowing.line(3), borrowing.line(7)]]
        )
        self.wait(4)

        self.play(borrowing.animate.to_edge(LEFT))
        self.play(GrowArrow(borrowing.point_to_line(2)))
        heap = Heap().to_edge(RIGHT, buff=2).to_edge(UP, buff=0.1)
        ferris = heap.allocate("Ferris")
        stack = Stack().to_edge(RIGHT, buff=2).to_edge(DOWN, buff=0.1)
        x = stack.push_named("x", "len: 6", "start: @")
        x_arrow = x.pointer(1, ferris[0].get_edge_center(DOWN))
        self.play(*[FadeIn(e) for e in [heap, stack, x_arrow]])
        self.play(borrowing.animate.point_to_next_line())
        self.wait(10)

        self.play(borrowing.animate.point_to_line(7))
        y = stack.push_named("y", "@")
        y_arrow = y.pointer(0, (x.top_left_corner() + x.get_edge_center(UP)) / 2, from_edge_center=LEFT)
        self.play(Create(y))
        self.play(DrawBorderThenFill(y_arrow))
        self.wait(2)

        self.play(borrowing.animate.point_to_next_line())
        self.play(borrowing.animate.point_to_next_line())
        self.wait(4)
        stack.pop()
        self.play(*[FadeOut(e) for e in [y_arrow, y]])
        self.play(borrowing.animate.point_to_line(4))
        self.wait(4)

        self.wait(2)
