fn main() {
    let x = String::from("Ferris");
    {
        let y = x;
        println!("Hello {}", &y);
    }
    println!("Bye");
}
