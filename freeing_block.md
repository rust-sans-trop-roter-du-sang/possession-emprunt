# Script

Un autre intérêt de la notion de propriété de la donnée est de déterminer de façon claire et prédictible quand la
libération de mémoire a lieu. La règle est très simple : la libération a lieu lorsque la variable propriétaire de la
donnée meurt. Ce petit programme va nous servir à illustrer cette règle.

Vous avez probablement anticipé ce qu'il se passe ligne 2.

Ligne 4, la variable y est définie dans un bloc de code délimité par des accolades. Sa durée de vie est limitée à ce
bloc.

Lorsqu'on arrive ligne 6, la variable y meurt et la mémoire associée est libérée sur le champ.

Pour rappel, il n'y a pas de garbage collector en Rust, la mémoire est vraiment libérée au moment de quitter le bloc de
code. Autrement dit, pour les habitués du C, une accolade fermante implique un appel automatique à la fonction free pour
toutes les variables définies dans le bloc.

Arrivés ligne 7, la variable x ne doit surtout pas être utilisée, car son start pointe vers de la mémoire maintenant
désallouée, voire potentiellement allouée pour autre chose. Le compilateur Rust se charge de vérifier que nous ne
faisons pas cette erreur.