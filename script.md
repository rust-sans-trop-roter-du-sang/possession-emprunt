# Script

## Création de variable

Voyons ensemble comment tout ça se met en place avec un premier petit programme
qui créé une String nommée x et s'en sert pour afficher 2 messages sur la sortie
standard.

Nous allons suivre l'exécution de ce programme pas-à-pas et cette flèche
permettra de savoir quelle instruction est en cours d'exécution.

C'est parti ! Que se passe-t-il du côté de la mémoire pendant l'exécution de
cette instruction ? Notre programme va interagir avec 2 zones mémoires : le tas
et la pile.

Le tas est l'endroit où on va allouer à la volée des zones mémoires dont la
taille n'est pas connue à la compilation : par exemple une chaine de caractères
qui serait tapée au clavier par notre utilisateur. La structure de l'ensemble
est bien imagée par son nom : c'est un tas sans vraiment d'organisation. On
accède à une zone donnée par son adresse en mémoire.

En l'occurrence, notre programme alloue assez de mémoire pour contenir la suite
de caractères F E 2 R I S.

La pile est l'endroit où vont, entre autres, être créées les variables du
programme et les paramètres d'appel des fonctions. Ici aussi, la structure de
l'ensemble est bien imagée par son nom : c'est une pile sur laquelle on pose des
choses et où les premiers éléments enlevés sont les derniers à y avoir été
posés. Dernière contrainte, seuls des éléments de taille connue à la compilation
peuvent être créés sur la pile.

En l'occurrence, notre programme crée de quoi finir de stocker notre String dont
sa longueur actuelle et l'adresse mémoire sur le tas où trouver les caractères
qui la composent.

## Déplacement

Reprenons le même programme pour notre deuxième exemple, avec une petite
modification : la création d'une variable y à partir de x et son utilisation en
lieu et place de x dans l'affichage des messages.

Nous avons déjà vu ce qu'il se passe en mémoire avec la ligne 2.

La ligne 3 copie la valeur de la variable x pour créer une variable y sur le
dessus de la pile. Notez que seules les informations directement dans x sont
copiées, c'est-à-dire que les caractères F E 2 R I S sur le tas ne sont pas
copiés : x et y pointent toutes les deux dessus.

Dans la langue de Rust, on vient de déplacer la propriété des données de x vers
y et nous n'avons plus le droit d'utiliser la variable x pour manipuler les
données.

Si jamais on essayait quand même de le faire, le compilateur le détecterait et
émettrait un message d'erreur, faisant échouer la compilation.

## Lecture sale

Considérons le petit programme suivant. Il est très semblable au précédent, à
ceci près que la déclaration de y autorise la modification.

Jusqu'à la ligne 3, il n'y a pas de surprise par rapport à ce qu'on vient de
voir.

Sur la ligne 4, vous avez peut-être deviné qu'on tronque la String y à 2
caractères.

Si Rust nous laissait compiler ce programme, la ligne suivante tenterait
d'afficher une String supposée de longueur 6 alors que la suite de caractère n'
est plus que de longueur 2. Au-delà du bug évident, il y a de réelles
implications en termes de sécurité ici : nous venons de mettre en place les
conditions d'un débordement de tampon.

## Libération (1/2)

Un autre intérêt de la notion de propriété de la donnée est de déterminer de
façon claire et prédictible quand la libération de mémoire a lieu. La règle est
très simple : la libération a lieu lorsque la variable propriétaire de la donnée
meurt. Ce petit programme va nous servir à illustrer cette règle.

Vous avez probablement anticipé ce qu'il se passe ligne 2.

Ligne 4, la variable y est définie dans un bloc de code délimité par des
accolades. Sa durée de vie est limitée à ce bloc.

Lorsqu'on arrive ligne 6, la variable y meurt et la mémoire associée est libérée
sur le champ.

Pour rappel, il n'y a pas de garbage collector en Rust, la mémoire est vraiment
libérée au moment de quitter le bloc de code. Autrement dit, pour les habitués
du C, une accolade fermante implique un appel automatique à la fonction free
pour toutes les variables définies dans le bloc.

Arrivés ligne 7, la variable x ne doit surtout pas être utilisée, car son start
pointe vers de la mémoire maintenant désallouée, voire potentiellement allouée
pour autre chose. Le compilateur Rust se charge de vérifier que nous ne faisons
pas cette erreur.

## Libération (2/2)

Et pour les appels de fonction alors ? C'est la même chose : une fonction n'est
pas grand-chose de plus qu'un bloc de code qui porte un nom. Le passage de la
variable x en paramètre déplace la propriété vers la variable y de la fonction
greet. Lorsque celle-ci termine son exécution, la mémoire allouée à la variable
y est libérée et, comme juste avant, il ne faut surtout pas utiliser la variable
x. Le compilateur Rust se chargera de vérifier ce point.

## Emprunt

Comment faire pour pouvoir continuer à utiliser x après l'avoir passé en
paramètre à greet ? En ne permettant à la fonction greet que d'emprunter x au
lieu d'en prendre possession !

Cet emprunt, comme pour l'appartement de tout à l'heure, permettra à greet
d'utiliser x le temps de son exécution et de la rendre ensuite. Ça se
matérialise dans le code par le fait de fournir une référence vers x, son
adresse en mémoire, au lieu de la valeur de x elle-même.

La fonction greet déclare que son paramètre y est une référence, un emprunt en
lecture seule, vers une String.

Une fois l'exécution de greet terminée, la variable y meurt : l'emprunt est
terminé. Puisque y n'était pas propriétaire de la donnée, x continue d'être
valide et peut être utilisée par la suite du programme.

## Emprunt en écriture

Nous venons de voir les références simples, c'est-à-dire en lecture seule.
Voyons maintenant comment obtenir une référence mutable.

Premier changement, pour espérer obtenir une référence mutable, il faut d'abord
que la variable dont on souhaite emprunter les données soit elle-même déclarée
comme mutable. Tous les contrôles de mutabilité et d'emprunt étant effectués par
le compilateur, il n'y a aucun impact sur la représentation en mémoire à
l'exécution.

On peut alors créer une référence mutable et la fournir à la fonction greet : on
donne les clefs de l'appartement aux ouvriers pour qu'ils puissent casser les
murs. L'emprunt mutable se termine quand la variable y meurt, à la fin de
l'exécution de la fonction greet.

Les ouvriers ont rendu les clefs, la variable x peut continuer à être utilisée
dans la suite du programme.