# Script

Considérons le petit programme suivant. Il est très semblable au précédent, à ceci près que la déclaration de y autorise
la modification.

Jusqu'à la ligne 3, il n'y a pas de surprise par rapport à ce qu'on vient de voir.

Sur la ligne 4, vous avez peut-être deviné qu'on tronque la String y à 2 caractères.

Si Rust nous laissait compiler ce programme, la ligne suivante tenterait d'afficher une String supposée de longueur 6
alors que la suite de caractère n'est plus que de longueur 2. Au-delà du bug évident, il y a de réelles implications 
en termes de sécurité ici : nous venons de mettre en place les conditions d'un débordement de tampon.