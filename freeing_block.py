from code import *


class Freeing(Scene):
    def construct(self):
        freeing = RustCode.from_file("freeing_block.rs")

        self.play(Write(freeing))
        self.wait(15)

        self.play(freeing.animate.to_edge(LEFT))
        heap = Heap().to_edge(RIGHT, buff=2).to_edge(UP, buff=0.1)
        ferris = heap.allocate("Ferris")
        stack = Stack().to_edge(RIGHT, buff=2).to_edge(DOWN, buff=0.1)
        x = stack.push_named("x", "len: 6", "start: @")
        x_arrow = x.pointer(1, ferris[0].get_edge_center(DOWN))
        self.play(GrowArrow(freeing.point_to_line(2)))
        self.play(*[FadeIn(e) for e in [heap, stack, x_arrow]])

        self.play(freeing.animate.point_to_line(4))
        y = stack.push_named("y", "len: 6", "start: @")
        y_arrow = y.pointer(1, ferris[0].get_edge_center(DOWN))
        self.play(Create(y), Create(y_arrow), x.animate.invalidate())
        self.wait(6)

        self.play(freeing.animate.point_to_line(6))
        self.wait(3)

        stack.pop()
        self.play(*[FadeOut(e) for e in [y, y_arrow, ferris]])
        self.wait(13)

        self.play(freeing.animate.point_to_next_line())
        self.wait(12)

        self.wait(2)
