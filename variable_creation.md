# Script

Voyons ensemble comment tout ça se met en place avec un premier petit programme qui créé une String nommée x et s'en
sert pour afficher 2 messages sur la sortie standard.

Nous allons suivre l'exécution de ce programme pas-à-pas et cette flèche permettra de savoir quelle instruction est
en cours d'exécution.

C'est parti ! Que se passe-t-il du côté de la mémoire pendant l'exécution de cette instruction ? Notre programme va
interagir avec 2 zones mémoires : le tas et la pile.

Le tas est l'endroit où on va allouer à la volée des zones mémoires dont la taille n'est pas connue à la compilation :
par exemple une chaine de caractères qui serait tapée au clavier par notre utilisateur. La structure de l'ensemble est
bien imagée par son nom : c'est un tas sans vraiment d'organisation. On accède à une zone donnée par son adresse en
mémoire.

En l'occurrence, notre programme alloue assez de mémoire pour contenir la suite de caractères F E 2 R I S.

La pile est l'endroit où vont, entre autres, être créées les variables du programme et les paramètres d'appel des
fonctions. Ici aussi, la structure de l'ensemble est bien imagée par son nom : c'est une pile sur laquelle on pose des
choses et où les premiers éléments enlevés sont les derniers à y avoir été posés. Dernière contrainte, seuls des
éléments de taille connue à la compilation peuvent être créés sur la pile.

En l'occurrence, notre programme crée de quoi finir de stocker notre String dont sa longueur actuelle et l'adresse
mémoire sur le tas où trouver les caractères qui la composent.