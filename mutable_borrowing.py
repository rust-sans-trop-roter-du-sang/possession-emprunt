from code import *


class MutableBorrowing(Scene):
    def construct(self):
        borrowing = RustCode.from_file("mutable_borrowing.rs")

        self.play(Write(borrowing))
        self.wait(4)

        self.play(borrowing.animate.to_edge(LEFT))
        self.play(GrowArrow(borrowing.point_to_line(2)))
        heap = Heap().to_edge(RIGHT, buff=2).to_edge(UP, buff=0.1)
        ferris = heap.allocate("Ferris")
        stack = Stack().to_edge(RIGHT, buff=2).to_edge(DOWN, buff=0.1)
        x = stack.push_named("x", "len: 6", "start: @")
        x_arrow = x.pointer(1, ferris[0].get_edge_center(DOWN))
        self.wait(11)
        self.play(*[FadeIn(e) for e in [heap, stack, x_arrow]])
        self.wait()

        self.play(borrowing.animate.point_to_next_line())
        self.wait(2)
        self.play(borrowing.animate.point_to_line(7))
        y = stack.push_named("y", "@")
        y_arrow = y.pointer(0, x.top_left_corner(), from_edge_center=LEFT)
        self.play(Create(y), DrawBorderThenFill(y_arrow))
        self.play(borrowing.animate.point_to_next_line())
        self.play(Transform(ferris, allocated_memory("FERRIS").move_to(ferris)))
        self.play(borrowing.animate.point_to_line(10))
        stack.pop()
        self.play(*[FadeOut(e) for e in [y_arrow, y]])
        self.wait(4)

        self.play(borrowing.animate.point_to_line(4))
        self.wait(3)

        self.wait(2)
