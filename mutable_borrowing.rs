fn main() {
    let mut x = String::from("Ferris");
    greet(&mut x);
    println!("Bye {}", &x);
}

fn greet(y: &mut String) {
    y.make_ascii_uppercase();
    println!("Hello {}", y);
}