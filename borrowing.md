# Script

Comment faire pour pouvoir continuer à utiliser x après l'avoir passé en paramètre à greet ? En ne permettant à la
fonction greet que d'emprunter x au lieu d'en prendre possession !

Cet emprunt, comme pour l'appartement de tout à l'heure, permettra à greet d'utiliser x le temps de son exécution et de
la rendre ensuite. Ça se matérialise dans le code par le fait de fournir une référence vers x, son adresse en 
mémoire, au lieu de la valeur de x elle-même.

La fonction greet déclare que son paramètre y est une référence, un emprunt en lecture seule, vers une String.

Une fois l'exécution de greet terminée, la variable y meurt : l'emprunt est terminé. Puisque y n'était pas 
propriétaire de la donnée, x continue d'être valide et peut être utilisée par la suite du programme.
