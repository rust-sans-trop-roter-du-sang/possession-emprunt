from code import *


class Freeing(Scene):
    def construct(self):
        freeing = RustCode.from_file("freeing_function.rs")

        self.play(Write(freeing))
        self.wait(2)

        self.play(freeing.animate.to_edge(LEFT))
        self.play(GrowArrow(freeing.point_to_line(3)))
        heap = Heap().to_edge(RIGHT, buff=2).to_edge(UP, buff=0.1)
        ferris = heap.allocate("Ferris")
        stack = Stack().to_edge(RIGHT, buff=2).to_edge(DOWN, buff=0.1)
        x = stack.push_named("x", "len: 6", "start: @")
        x_arrow = x.pointer(1, ferris[0].get_edge_center(DOWN))
        self.play(*[FadeIn(e) for e in [heap, stack, x_arrow]])
        self.wait(2)

        self.play(freeing.animate.point_to_line(7))
        y = stack.push_named("y", "len: 6", "start: @")
        y_arrow = y.pointer(1, ferris[0].get_edge_center(DOWN))
        self.play(Create(y), Create(y_arrow), x.animate.invalidate())
        self.wait(2)

        self.play(freeing.animate.point_to_line(9))
        self.wait(2)

        stack.pop()
        self.play(*[FadeOut(e) for e in [y, y_arrow, ferris]])
        self.wait(1)

        self.play(freeing.animate.point_to_line(4))
        self.wait(6)

        self.wait(2)
