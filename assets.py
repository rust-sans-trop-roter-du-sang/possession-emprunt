from manim import ImageMobject


def ferris_exclamation():
    return ImageMobject("assets/ferris exclamation.png").scale(0.7)
