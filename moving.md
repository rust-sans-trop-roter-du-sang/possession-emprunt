# Script

Reprenons le même programme pour notre deuxième exemple, avec une petite modification : la création d'une
variable y à partir de x et son utilisation en lieu et place de x dans l'affichage des messages.

Nous avons déjà vu ce qu'il se passe en mémoire avec la ligne 2.

La ligne 3 copie la valeur de la variable x pour créer une variable y sur le dessus de la pile. Notez que seules les
informations directement dans x sont copiées, c'est-à-dire que les caractères F E 2 R I S sur le tas ne sont pas
copiés : x et y pointent toutes les deux dessus.

Dans la langue de Rust, on vient de déplacer la propriété des données de x vers y et nous n'avons plus le droit
d'utiliser la variable x pour manipuler les données.

Si jamais on essayait quand même de le faire, le compilateur le détecterait et émettrait un message d'erreur, faisant
échouer la compilation.