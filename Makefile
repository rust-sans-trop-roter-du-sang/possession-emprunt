SCENES ?= variable_creation moving dirty_read freeing_block freeing_function borrowing mutable_borrowing
QUALITY ?= h

BASE_OUT := out
OUT := $(BASE_OUT)/$(QUALITY)
VIDEOS := $(foreach scene,$(SCENES),$(OUT)/$(scene).mp4)
MERGED := $(OUT)/merged.mp4
CONCAT := "concat:$(shell echo $(VIDEOS) | sed 's/ /|/g')"

.PHONY: all
all: $(VIDEOS)

.PHONY: merged
merged: $(MERGED)

$(MERGED): $(VIDEOS)
	ffmpeg -i $(CONCAT) -c copy $@

.PHONY: clean
clean:
	$(RM) -r media/
	$(RM) -r $(BASE_OUT)/

$(OUT)/%.mp4: %.py manim.cfg | $(OUT)
	manim $(MANIM_FLAGS) --config_file manim.cfg --quality $(QUALITY) --output_file $@ $<

$(OUT):
	mkdir -p $@

