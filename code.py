from abc import ABC

from manim import *

_SCALE_FACTOR = .7
_RUST: str = "#e43717"
_LABEL_FONT = "Open Sans"
_CODE_FONT = "Fira Code"


class RustCode(VGroup, ABC):
    def __init__(self, code: Code, execution_pointer: Arrow, initial_line_number=0, **kwargs):
        super().__init__(**kwargs)
        if not code.insert_line_no or initial_line_number > len(code.line_numbers):
            raise ValueError()
        self.name = code.file_name
        self._code = code
        self._current_line_number = initial_line_number
        self._pointer = execution_pointer
        self.add(self._code)

    @classmethod
    def from_file(cls, file_name: str, **kwargs) -> 'RustCode':
        return RustCode(
            code=Code(
                background="window",
                file_name=file_name,
                font="Monospace",
                insert_line_no=True,
                language="rust",
                style="manni",
                tab_width=4,
            ),
            execution_pointer=Arrow(
                max_stroke_width_to_length_ratio=100,
                stroke_width=20).set_length(0.5).set_color(_RUST),
            **kwargs
        )

    def _current_line(self):
        len(self._code.line_numbers)
        return self._code.line_numbers.chars[self._current_line_number]

    def line(self, n: int) -> Text:
        return self._code.code.chars[n - 1]

    def pointer(self):
        self.add(self._pointer.next_to(self._current_line(), LEFT))
        return self._pointer

    def point_to_next_line(self) -> Arrow:
        self._current_line_number += 1
        return self._pointer.next_to(self._current_line(), LEFT)

    def point_to_line(self, n: int) -> Arrow:
        self._current_line_number = n - 1
        return self.pointer()


class Element(VGroup, ABC):
    def __init__(self, *value: str):
        super().__init__()
        self._rect = Rectangle().scale(_SCALE_FACTOR)
        self._content = Paragraph(*value, alignment="left", font=_CODE_FONT).scale(_SCALE_FACTOR)
        self._name = None
        self._pointers = []
        self.set_color(DARK_BLUE)
        self.add(self._rect, self._content)

    @classmethod
    def new_with_name(cls, name: str, *value: str) -> 'Element':
        element = Element(*value)
        element.named(name)
        return element

    def set_color(self, color: str, family=True) -> 'Element':
        self.color = color
        self._rect.set_color(color).set_fill(color, 0.3)
        self._content.set_color(color)
        if self._name:
            self._name.set_color(color)
        for p in self._pointers:
            p.set_color(color)
        return self

    def invalidate(self) -> 'Element':
        return self.set_color(DARK_GRAY)

    def content_line(self, n: int):
        return self._content.chars.submobjects[n]

    def top_left_corner(self):
        return self._rect.get_corner(UL)

    def named(self, name: str) -> Text:
        self._name = Text(name, color=DARK_BLUE, font=_LABEL_FONT).scale(_SCALE_FACTOR).next_to(self._rect, LEFT)
        self.add(self._name)
        return self._name

    def pointer(self, from_content_line: int, to_point: Any, from_edge_center=RIGHT) -> CurvedArrow:
        arrow = CurvedArrow(
            start_point=self.content_line(from_content_line).get_edge_center(from_edge_center),
            end_point=to_point
        ).set_color(self.color)
        self._pointers += arrow
        return arrow


class Stack(VGroup, ABC):
    def __init__(self, name="Pile", color=DARK_BLUE):
        super().__init__()
        self._name = Text(name, color=color, font=_LABEL_FONT).scale(_SCALE_FACTOR)
        self._elements: VGroup = VGroup()
        self.add(self._name, self._elements)

    def push(self, *value: str) -> Element:
        return self._push(Element(*value))

    def push_named(self, name: str, *value: str) -> Element:
        return self._push(Element.new_with_name(name, *value))

    def _push(self, element: Element) -> Element:
        self._elements.add(element)
        self._elements.arrange(UP, buff=0)
        self._elements.next_to(self._name, UP)
        return element

    def pop(self): return self._elements.submobjects.pop()


def allocated_memory(text: str) -> VGroup:
    mem = VGroup()
    for c in text:
        mem += CharCell(c).scale(_SCALE_FACTOR)
    mem.arrange(RIGHT, buff=0)
    return mem


class Heap(VGroup, ABC):
    def __init__(self, name="Tas", color=_RUST, font=_LABEL_FONT):
        super().__init__()
        self._name = Text(name, color=color, font=font).scale(_SCALE_FACTOR)
        self.add(self._name)

    def allocate(self, text: str) -> VGroup:
        allocated = allocated_memory(text)
        allocated.next_to(self[-1], DOWN)
        self.add(allocated)
        return allocated


class CharCell(VGroup, ABC):
    def __init__(self, value: str, color=_RUST):
        super().__init__()
        text = Text(value, color=color, font=_CODE_FONT)
        square = Square(side_length=0.8, color=color) \
            .set_fill(color, opacity=0.3) \
            .next_to(text.get_bottom(), UP, buff=-0.17)
        self.add(square, text)
