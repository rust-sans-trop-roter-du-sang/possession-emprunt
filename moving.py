from manim import *

from assets import ferris_exclamation
from code import Heap, Stack, RustCode


class Moving(Scene):
    def construct(self):
        moving1 = RustCode.from_file("moving1.rs")
        moving2 = RustCode.from_file("moving2.rs")

        self.play(Write(moving1))
        self.wait(2)
        assign_line = moving2.line(3)
        self.play(Write(assign_line))
        self.wait()
        ref_line1 = moving2.line(4)
        ref_line2 = moving2.line(5)
        self.play(AnimationGroup(
            Transform(moving1.line(4), ref_line1),
            Transform(moving1.line(5), ref_line2),
            run_time=2
        ))
        self.wait(2)

        self.clear()
        self.add(moving2)
        self.play(moving2.animate.to_edge(LEFT))
        heap = Heap().to_edge(RIGHT, buff=2).to_edge(UP, buff=0.1)
        ferris = heap.allocate("Ferris")
        stack = Stack().to_edge(RIGHT, buff=2).to_edge(DOWN, buff=0.1)
        x = stack.push_named("x", "len: 6", "start: @")
        arrow = x.pointer(1, ferris[0].get_edge_center(DOWN))
        self.play(GrowArrow(moving2.point_to_line(2)))
        self.play(*[FadeIn(e) for e in [heap, stack, arrow]])
        self.wait()

        self.play(moving2.animate.point_to_next_line())
        y = stack.push_named("y", "len: 6", "start: @")
        self.play(Write(y))
        self.wait(11)
        arrow = y.pointer(1, ferris[0].get_edge_center(DOWN))
        self.play(DrawBorderThenFill(arrow))

        self.wait(5)
        self.play(x.animate.invalidate())
        self.wait(4)

        moving1 = RustCode.from_file("moving1.rs").to_edge(LEFT)
        self.play(Circumscribe(ref_line2, color=RED_E, buff=0.01, run_time=1.5))
        invalid_line = moving1.line(5)
        self.play(Transform(ref_line2, invalid_line))
        self.wait(2)
        self.play(
            FadeIn(ferris_exclamation().next_to(invalid_line, RIGHT)),
            FadeIn(ferris_exclamation().next_to(x, LEFT))
        )
        self.wait(2)

        self.wait(3)
